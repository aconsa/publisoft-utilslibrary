package aconsa.publisoft.utilslibrary.exception;

public class Error404Exception extends RuntimeException {

    public Error404Exception(String message) {
        super(message);
    }
}
