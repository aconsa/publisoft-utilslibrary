package aconsa.publisoft.utilslibrary.exception;

public class JWTDecodeException extends RuntimeException {
    public JWTDecodeException(String message) {
        super(message);
    }
}
