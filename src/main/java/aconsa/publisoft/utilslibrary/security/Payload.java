package aconsa.publisoft.utilslibrary.security;

import java.io.Serializable;
import java.util.List;

public class Payload implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<String> aud;
    private List<String> scope;
    private Long exp;
    private List<String> authorities;
    private String jti;
    private String client_id;

    public Payload() {
    }

    public Payload(List<String> aud, List<String> scope, Long exp, List<String> authorities, String jti, String client_id) {
        this.aud = aud;
        this.scope = scope;
        this.exp = exp;
        this.authorities = authorities;
        this.jti = jti;
        this.client_id = client_id;
    }

    @Override
    public String toString() {
        return "Payload{" +
                "aud=" + aud +
                ", scope=" + scope +
                ", exp=" + exp +
                ", authorities=" + authorities +
                ", jti='" + jti + '\'' +
                ", client_id='" + client_id + '\'' +
                '}';
    }

    public List<String> getAud() {
        return aud;
    }

    public void setAud(List<String> aud) {
        this.aud = aud;
    }

    public List<String> getScope() {
        return scope;
    }

    public void setScope(List<String> scope) {
        this.scope = scope;
    }

    public Long getExp() {
        return exp;
    }

    public void setExp(Long exp) {
        this.exp = exp;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }
}
