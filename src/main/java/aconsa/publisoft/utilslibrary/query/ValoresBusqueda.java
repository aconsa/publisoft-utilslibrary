package aconsa.publisoft.utilslibrary.query;

public class ValoresBusqueda {

    private String codigo;
    private String valor;

    public ValoresBusqueda() {
    }

    public ValoresBusqueda(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "ValoresBusqueda{" +
                "codigo='" + codigo + '\'' +
                ", valor='" + valor + '\'' +
                '}';
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
