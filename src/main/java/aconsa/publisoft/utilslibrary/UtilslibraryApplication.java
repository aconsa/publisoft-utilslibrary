package aconsa.publisoft.utilslibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UtilslibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(UtilslibraryApplication.class, args);
	}

}
