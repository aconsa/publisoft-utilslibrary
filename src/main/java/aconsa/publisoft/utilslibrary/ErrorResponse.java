package aconsa.publisoft.utilslibrary;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import java.util.List;

public class ErrorResponse {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp;
    private Integer code;
    private String message;
    private List<String> errors;

    public ErrorResponse(LocalDateTime timestamp, Integer code, String message) {
        this.timestamp = timestamp;
        this.code = code;
        this.message = message;
    }

    public ErrorResponse(LocalDateTime timestamp, Integer code, List<String> errors) {
        this.timestamp = timestamp;
        this.code = code;
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "timestamp=" + timestamp +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", errors=" + errors +
                '}';
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
