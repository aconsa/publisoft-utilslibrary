package aconsa.publisoft.utilslibrary;

public enum Estado {

    GUARDADO("G"),
    ANULADO("A");

    public final String valor;

    Estado(String valor) {
        this.valor = valor;
    }
}
