package aconsa.publisoft.utilslibrary;

public class GeneralMessages {

    public static final String GUARDADO = "El registro de %s se guardo con exito.";
    public static final String ERROR_AL_GUARDAR = "%s ya existe, no se puede guardar de nuevo.";
    public static final String ERROR_GUARDANDO = "Error al guardar.";
    public static final String INICIO_GUARDAR_LOG = "Iniciando guardar {} con codigo: {}";
    public static final String GUARDADO_EXITOSO_LOG = "{} guardado con exito: {}";
    public static final String INICIO_GUARDAR_DB_LOG = "Guardando {} en base de datos con codigo: {}";
    public static final String GUARDADO_EXITOSO_DB_LOG = "{} guardado en base de datos con exito: {}";

    public static final String ACTUALIZADO = "El registro de %s se actualizó con exito.";
    public static final String ERROR_AL_ACTUALIZAR = "%s no se pudo actualizar.";
    public static final String ERROR_ACTUALIZANDO = "Error al actualizar.";
    public static final String ERROR_ACTUALIZAR_LOG = "No se encontraron datos para actualizar: {}";
    public static final String INICIO_ACTUALIZAR_LOG = "Iniciando actualizar {} con codigo: {}";
    public static final String ACTUALIZADO_CON_EXITO_LOG = "{} actualizado con exito: {}";
    public static final String INICIO_ACTUALIZAR_DB_LOG = "Actualizando {} en base de datos con codigo: {}";
    public static final String ACTUALIZADO_CON_EXITO_DB_LOG = "{} actualizado en base de datos con exito: {}";

    public static final String ANULADO = "Se ha anulado el recurso %s";
    public static final String ERROR_ANULANDO = "Error al anular.";
    public static final String ERROR_ANULANDO_LOG = "No se encontraron datos para anular: {}";
    public static final String INICIO_ANULAR_LOG = "Iniciando anular {} con codigo: {}";
    public static final String ANULADO_CON_EXITO_LOG = "{} anulado con exito: {}";
    public static final String INICIO_ANULAR_DB_LOG = "Anulando {} en base de datos con codigo: {}";
    public static final String ANULADO_CON_EXITO_DB_LOG = "{} anulado en base de datos con exito: {}";

    public static final String ELIMINADO = "El registro de %s se elimino con exito.";
    public static final String ERROR_ELIMINANDO = "Error al eliminar.";

    public static final String ENCONTRADO = "Se ha encontrado el recurso %s";
    public static final String ERROR_AL_BUSCAR = "No se encontraron datos guardados de %s.";
    public static final String ERROR_AL_BUSCAR_CON_PARAMETROS = "No se encontraron datos de %s con los parámetros (%s).";
    public static final String REGISTROS_OBTENIDOS_DE_DB = "{} registros obtenidos de la base de datos.";
    public static final String INICIO_BUSQUEDA_AVANZADA_LOG = "Iniciando busqueda avanzada de {} con valores: {}";
    public static final String REGISTROS_BUSQUEDA_AVANZADA_LOG = "Se encontraron {} registros";

    public static final String ERROR_500_CON_NOTIFICACIÓN_AUTOMÁTICA = "Error interno del servidor, el equipo de soporte será notificado automáticamente, por favor mantener la calma.";
    public static final String ERROR_500_SIN_NOTIFICACIÓN_AUTOMÁTICA = "Error interno del servidor, comuníquese con el equipo de soporte.";

    public static final String NOTIFICACIÓN_AL_EQUIPO_DE_APOYO = "¡Nuevo error en el recurso %s!.";
    public static final String INFORME_DE_ERRORES = "Nuevo error.";
    public static final String ERROR_CORREO_EXISTENTE = "%s ya existe con el correo: %s";
    public static final String DESCRIPCION_SUCURSAL_DEFAULT = "Publisoft";
    public static final String CORREO_BIENVENIDA = "Te damos la bienvenida %s";
    public static final String CORREO_VERIFICACION = "Correo de verificación";
    public static final String CORREO_TEMPLATE_CAMBIO_CONTRASENA = "cambiar constraseña";
    public static final String CORREO_TEMPLATE_CREAR_CUENTA = "crear una cuenta";
    public static final String CORREO_TEMPLATE_REPORTAR = "por favor, comuníquese con el equipo de soporte.";
    public static final String CORREO_TEMPLATE_IGNORAR = "por favor, ignore este correo.";

    private GeneralMessages() throws IllegalAccessException {
        throw new IllegalAccessException("Utility class");
    }
}
